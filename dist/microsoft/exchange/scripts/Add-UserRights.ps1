[CmdletBinding()]
param(
    [Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainUser
)

begin {
}

process {
    $rights = @(
        'SeTakeOwnershipPrivilege',
        'SeChangeNotifyPrivilege',
        'SeLockMemoryPrivilege',
        'SeManageVolumePrivilege',
        'SeAssignPrimaryTokenPrivilege',
        'SeSystemtimePrivilege',
        'SeTimeZonePrivilege',
        'SeCreatePagefilePrivilege',
        'SeCreateTokenPrivilege',
        'SeCreateGlobalPrivilege',
        'SeCreatePermanentPrivilege',
        'SeCreateSymbolicLinkPrivilege',
        'SeDebugPrivilege',
        'SeEnableDelegationPrivilege',
        'SeRemoteShutdownPrivilege',
        'SeAuditPrivilege',
        'SeImpersonatePrivilege',
        'SeIncreaseWorkingSetPrivilege',
        'SeIncreaseBasePriorityPrivilege',
        'SeLoadDriverPrivilege',
        'SeSecurityPrivilege',
        'SeRelabelPrivilege',
        'SeSystemEnvironmentPrivilege',
        'SeProfileSingleProcessPrivilege',
        'SeSystemProfilePrivilege',
        'SeUndockPrivilege',
        'SeRestorePrivilege',
        'SeShutdownPrivilege',
        'SeSyncAgentPrivilege',
        'SeTcbPrivilege',
        'SeTrustedCredManAccessPrivilege',
        'SeIncreaseQuotaPrivilege',
        'SeBackupPrivilege',
        'SeMachineAccountPrivilege',
        'SeInteractiveLogonRight',
        'SeNetworkLogonRight',
        'SeServiceLogonRight',
        'SeBatchLogonRight',
        'SeRemoteInteractiveLogonRight'
    )

    foreach ($right in $rights) {
        Grant-UserRight -Account $DomainUser -Right $right
    }
}