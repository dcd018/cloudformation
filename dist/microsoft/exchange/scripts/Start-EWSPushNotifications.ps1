param(
    [Parameter(Position=0, Mandatory=$true)]
    [System.String]
    $DomainDNSName = "",

    [Parameter(Position=1, Mandatory=$true)]
    [System.String]
    $MailboxUser = "",

    [Parameter(Position=2, Mandatory=$true)]
    [System.String]
    $Folder = "",

    [Parameter(Position=3, Mandatory=$true)]
    [System.String]
    $Events = "",

    [Parameter(Position=4, Mandatory=$true)]
    [System.String]
    $LambdaFunction = "",
    
    [Parameter(Position=5, Mandatory=$true)]
    [int]
    $ListenPort = 36728,

    [Parameter(Position=6, Mandatory=$true)]
    [System.String]
    $Region
)

$args = @(
    '--host', 'https://exch1/ews/exchange.asmx', '--local',
    '--listen-port', $ListenPort,
    '--mailbox', "$MailboxUser@$DomainDNSName",
    '--folder', $Folder,
    '--events', $Events,
    '--lambda-function', $LambdaFunction,
    '--disable-logging',
    '--enable-event-logging',
    '--region', $Region
)

& "$PSScriptRoot\EWSPushNotifications.exe" "--%" "$args"