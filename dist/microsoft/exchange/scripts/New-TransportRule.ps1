[CmdletBinding()]
param(
	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainAdminUser,

	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainAdminPassword,
	
	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainDNSName,

	[Parameter(Mandatory=$true)]
	[System.String]
	$RuleName,

	[Parameter(Mandatory=$true)]
	[ValidateSet("InOrganization", "NotInOrganization", "ExternalPartner", "ExternalNonPartner")]
	[System.String]
    $SentToScope,
    
    [Parameter(Mandatory=$true)]
	[System.String]
    $RecipientAddressMatchPhrase,
    
    [Parameter(Mandatory=$true)]
	[System.String]
	$RedirectRecipientAddress
)

begin {
	$libPath = "$PSScriptRoot\lib\ExchangeManagementConsole"
	
	$args = @(
		'--host', 'exch1', 
		'--domain', $DomainDNSName,
		'--user', $DomainAdminUser,
		'--password', $DomainAdminPassword
	)

	$cmd = @(
		'New-TransportRule',
        '-Name', "'$RuleName'",
        '-RecipientAddressContainsWords', $RecipientAddressMatchPhrase,
		'-SentToScope', $SentToScope,
        '-RedirectMessageTo', $RedirectRecipientAddress
        '-Mode', 'Enforce'
	)

}

process {
	& "$libPath\ExchangeManagementConsole.exe" "--%" "$args --cmd `"$cmd`""
}