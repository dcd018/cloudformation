[CmdletBinding()]
param(
	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainAdminUser,

	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainAdminPassword,
	
	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainDNSName,

	[Parameter(Mandatory=$true)]
	[System.String]
	$Identity,

	[Parameter(Mandatory=$true)]
	[System.String]
	$User,

	[Parameter(Mandatory=$true)]
	[ValidateSet("DeleteItem", "ReadPermission", "ChangePermission", "ChangeOwner", "FullAccess", "ExternalAccount", "SendAs")]
	[System.String]
	$AccessRights
)

begin {
	$libPath = "$PSScriptRoot\lib\ExchangeManagementConsole"
	
	$args = @(
		'--host', 'exch1', 
		'--domain', $DomainDNSName,
		'--user', $DomainAdminUser,
		'--password', $DomainAdminPassword
	)

	$cmd = @(
		'Add-MailboxPermission',
		'-Identity', $Identity,
		'-User', $User,
		'-AccessRights', $AccessRights
	)

}

process {
	& "$libPath\ExchangeManagementConsole.exe" "--%" "$args --cmd `"$cmd`""
}