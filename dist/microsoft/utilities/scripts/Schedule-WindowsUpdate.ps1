[CmdletBinding()]
param(
	[Parameter(Mandatory=$false)]
	[System.String]
	$MailFromUser,

	[Parameter(Mandatory=$false)]
	[System.String]
	$MailToUser,

	[Parameter(Mandatory=$false)]
	[System.String]
	$SmtpServer,

	[Parameter(Mandatory=$false)]
	[System.String]
	$NetBIOSName = (Get-WmiObject win32_computersystem).DNSHostName,

	[Parameter(Mandatory=$false)]
	[System.String]
	$DomainDNSName = (Get-WmiObject win32_computersystem).Domain
)

begin {

	$MailFrom = "$MailFromUser@$DomainDNSName"
	$MailTo = "$MailToUser@$DomainDNSName"

	$Sta = New-ScheduledTaskAction -Execute "powershell.exe -ExecutionPolicy RemoteSigned -Command C:\cfn\scripts\Get-WindowsUpdate.ps1 -MailFrom $MailFrom -MailTo $MailTo -SmtpServer $SmtpServer -NetBiosName $NetBiosName -DomainDNSName $DomainDNSName"
	$Stt = New-ScheduledTaskTrigger -Daily -At 3am
}

process {
	Register-ScheduledTask -TaskName "RunWindowsUpdate" -Trigger $Stt -User "SYSTEM" -Action $Sta -AsJob -Force -RunLevel Highest
}