#[CmdletBinding()]
param(
	[Parameter(Mandatory=$false)]
	[System.String]
	$Site = "PowerShell",

	[Parameter(Mandatory=$true)]
	[ValidateSet("anonymous", "basic", "digest", "forms", "windows")]
	[System.String]
	$Type = 'basic',

	[Parameter(Mandatory=$false)]
	[bool]
	$Enabled = $true
)

begin {
	import-module WebAdministration
}

process {
	Set-WebConfigurationProperty -Filter "/system.webServer/security/authentication/$($Type)Authentication" -Name "Enabled" -Value $Enabled -PSPath IIS:\ -Location "Default Web Site/$Site"
	iisreset
}