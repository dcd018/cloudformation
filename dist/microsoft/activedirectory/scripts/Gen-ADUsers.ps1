[CmdletBinding()]
param(
	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$KmsKeyAlias = "",

	[Parameter(Position=1, Mandatory=$true)]
	[System.String]
	$BucketName = "",

	[Parameter(Position=2, Mandatory=$false)]
	[System.String]
	$UpnSuffix = [System.DirectoryServices.ActiveDirectory.Forest]::GetCurrentForest().RootDomain.Name
)

begin {
	Import-Module "$PSScriptRoot\Gen-RandomPassword.ps1"
	#Add-Type -AssemblyName System.web

	$kmsKey = Get-KMSKey -KeyId alias/$KmsKeyAlias | Select-Object "KeyId"
	$kmsKeyId = $kmsKey.KeyId

	$organizationalUnit = ("CN=users," + ([ADSI]"LDAP://RootDSE").defaultNamingContext)
	$users = Import-Csv $PSScriptRoot\users.csv
}

process {

	$exportUsers = @()

	foreach ($user in $users) {
		$password = Gen-ADPassword
		#$password = [System.Web.Security.Membership]::GeneratePassword(10,0)
		$userpwd = ConvertTo-SecureString -AsPlainText $password -Force

		$firstname = $user.firstname
		$lastname = $user.lastname
		$alias = $user.alias
		$changePassword = [int]$user.changePassword

		$name = "$firstname $lastname"
		$upn = "$alias@$UpnSuffix".ToLower()

		$exportUser = [pscustomobject]@{

			username = "$UpnSuffix\$alias"

			password  = $password
		}

		$exportUsers += $exportUser

		New-ADUser -Name $name `
		-GivenName $firstname `
		-Surname $lastname `
		-SamAccountName $alias `
		-DisplayName $name `
		-AccountPassword $userpwd `
		-PassThru `
		-Enabled $true `
		-UserPrincipalName $upn `
		-EmailAddress $upn `
		-ChangePasswordAtLogon $changePassword `
		-Path $OrganizationalUnit `
		-EA 0
	}

	$input = $exportUsers | ConvertTo-Json | Out-String
	
	$EncryptedOuput = (Invoke-KMSEncrypt -KeyId $kmsKeyId -Plaintext $(ConvertFrom-StringToMemoryStream $input))
	$EncryptedBase64 = ConvertFrom-StreamToBase64 -inputStream $EncryptedOuput.CiphertextBlob
	$EncryptedBase64 | Out-File -encoding utf8 -filepath $PSScriptRoot\exchange-mailboxes.json
	
	$key = "deploy/$KmsKeyAlias/.credentials/exchange-mailboxes.json"
	Write-S3Object -BucketName $BucketName -Key $key -File $PSScriptRoot\exchange-mailboxes.json
	Remove-Item $PSScriptRoot\exchange-mailboxes.json
}