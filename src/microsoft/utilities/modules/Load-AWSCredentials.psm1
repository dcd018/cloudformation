function Load-AWSCredentials {
    param()

    Import-Module AWSPowerShell
        
    $iam = (Invoke-WebRequest http://169.254.169.254/latest/meta-data/iam/security-credentials -UseBasicParsing).Content
    $iamProfileInfo = ConvertFrom-Json (Invoke-WebRequest http://169.254.169.254/latest/meta-data/iam/security-credentials/$iam -UseBasicParsing).Content
    Set-AWSCredentials -AccessKey $iamProfileInfo.AccessKeyId -SecretKey $iamProfileInfo.SecretAccessKey -SessionToken $iamProfileInfo.Token
}

function Get-AWSRegionFromMetadata {
    param()
    $az = (Invoke-WebRequest http://169.254.169.254/latest/meta-data/placement/availability-zone -UseBasicParsing).Content
    $region = $az -replace ".{1}$"
    return $region
}
