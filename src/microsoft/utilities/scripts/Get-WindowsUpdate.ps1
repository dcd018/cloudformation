[CmdletBinding()]
param(
	[Parameter(Mandatory=$false)]
	[Switch]
	$AutoReboot,

	[Parameter(Mandatory=$false)]
	[System.String]
	$MailFrom,

	[Parameter(Mandatory=$false)]
	[System.String]
	$MailTo,

	[Parameter(Mandatory=$false)]
	[System.String]
	$SmtpServer,

	[Parameter(Mandatory=$false)]
	[System.String]
	$NetBIOSName = (Get-WmiObject win32_computersystem).DNSHostName,

	[Parameter(Mandatory=$false)]
	[System.String]
	$DomainDNSName = (Get-WmiObject win32_computersystem).Domain
)

begin {
	Import-Module PSWindowsUpdate
	$fqdn = "$NetBIOSName.$DomainDNSName"
}

process {
	Get-WUInstall -AcceptAll -IgnoreReboot
	if ($AutoReboot) {
		Restart-Computer -Force
	} else {
		$rebootRequired = Get-WURebootStatus -Silent

		if ($rebootRequired -and $MailFrom -and $MailTo -and $SmtpServer) {
			$subject = "Reboot requiered for $fqdn"
			$body = "A reboot for $fqdn is required after installing updates."
			Send-MailMessage -From $MailFrom -To $MailTo -Subject $subject -Body $body -SmtpServer $SmtpServer
		}
	}
}