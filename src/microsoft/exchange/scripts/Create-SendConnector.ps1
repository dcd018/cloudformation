[CmdletBinding()]
param(
	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainAdminUser,

	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainAdminPassword,

	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainDNSName,

	[Parameter(Position=0, Mandatory=$false)]
	[System.String]
	$ConnectorName = 'Default'
)

begin {
	$libPath = "$PSScriptRoot\lib\ExchangeManagementConsole"
	$fqdn = "EXCH1.$DomainDNSName"
	
	$args = @(
		'--host', 'exch1', 
		'--domain', $DomainDNSName,
		'--user', $DomainAdminUser,
		'--password', $DomainAdminPassword
	)

	$cmd = @(
		'New-SendConnector', '-Internet',
		'-Name', "'$ConnectorName'",
		'-AddressSpace', "'*'",
		'-SourceTransportServers', 'EXCH1',
		'-Fqdn', $fqdn
	)
}

process {
	& "$libPath\ExchangeManagementConsole.exe" "--%" "$args --cmd `"$cmd`""
}