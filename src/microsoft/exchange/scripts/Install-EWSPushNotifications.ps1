[CmdletBinding()]
param(
    [Parameter(Position=0, Mandatory=$true)]
    [int]
    $ListenPort = 36728,

    [Parameter(Position=1)]
    [System.String]
    $Namespace = "EWSPushNotifications",

    [Parameter(Position=2, Mandatory=$true)]
    [System.String]
    $DomainDNSName,

    [Parameter(Position=3, Mandatory=$true)]
    [System.String]
    $SMSForwardUser
)

begin {
    $libPath = "$PSScriptRoot\lib"
    $sourcePath = "$libPath\EWSPushNotifications"
    $destPath = "c:\Users\Public\EWSPushNotifications"

    $machineName = [System.Net.DNS]::GetHostEntry("LocalHost").HostName
    $listenUri = @('http://', $machineName, ':', $ListenPort, '/', $Namespace, '/') -join ''
}

process {
    New-Item -ItemType directory -Path $destPath
    Copy-Item -Path "$sourcePath\*" $destPath -Recurse
    Copy-Item -Path "$PSScriptRoot\Start-EWSPushNotifications.ps1" $destPath
    
    Invoke-Expression "netsh http add urlacl url=$listenUri user=$DomainDNSName\$SMSForwardUser"

    if ([System.Diagnostics.EventLog]::SourceExists($Namespace) -eq $false) {
        [System.Diagnostics.EventLog]::CreateEventSource($Namespace, "Application")
    }
}