[CmdletBinding()]
param(
	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainAdminUser,

	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainAdminPassword,
	
	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainDNSName,

	[Parameter(Position=0, Mandatory=$false)]
	[System.String]
	$ConnectorName = 'Exchange Online Protection'
)

begin {
	$libPath = "$PSScriptRoot\lib\ExchangeManagementConsole"

	$args = @(
		'--host', 'exch1', 
		'--domain', $DomainDNSName,
		'--user', $DomainAdminUser,
		'--password', $DomainAdminPassword
	)

	$permissionGroups = @(
		'Partners',
		'AnonymousUsers'
	) -join ','
	
	$remoteIpRanges = @(
		'23.103.132.0/22',
		'23.103.136.0/21',
		'23.103.144.0/20',
		'23.103.198.0/23',
		'23.103.200.0/22',
		'23.103.212.0/22',
		'40.92.0.0/14',
		'40.107.0.0/17',
		'40.107.128.0/18',
		'52.100.0.0/14',
		'65.55.88.0/24',
		'65.55.169.0/24',
		'94.245.120.64/26',
		'104.47.0.0/17',
		'104.212.58.0/23',
		'134.170.132.0/24',
		'134.170.140.0/24',
		'157.55.234.0/24',
		'157.56.110.0/23',
		'157.56.112.0/24',
		'207.46.51.64/26',
		'207.46.100.0/24',
		'207.46.163.0/24',
		'213.199.154.0/24',
		'213.199.180.128/26',
		'216.32.180.0/23'
	) -join ','

	$cmd = @(
		'New-ReceiveConnector', '-Partner',
		'-name', "'$ConnectorName'",
		'-TransportRole', 'FrontendTransport',
		'-PermissionGroups', $permissionGroups,
		'-Bindings', '0.0.0.0:25',
		'-RemoteIPRanges', $remoteIpRanges
	)
}

process {
	& "$libPath\ExchangeManagementConsole.exe" "--%" "$args --cmd `"$cmd`""
}