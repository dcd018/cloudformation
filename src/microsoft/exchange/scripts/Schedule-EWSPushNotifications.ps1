[CmdletBinding()]
param(
    [Parameter(Position=1, Mandatory=$true)]
    [System.String]
    $DomainDNSName,

    [Parameter(Position=1, Mandatory=$true)]
    [System.String]
    $DomainNetBIOSName,

    [Parameter(Position=2, Mandatory=$true)]
    [System.String]
    $MailboxUser,

    [Parameter(Position=3, Mandatory=$true)]
    [System.String]
    $Folder,

    [Parameter(Position=4, Mandatory=$true)]
    [System.String]
    $Events,

    [Parameter(Position=5, Mandatory=$true)]
    [System.String]
    $LambdaFunction,

    [Parameter(Position=6, Mandatory=$true)]
    [int]
    $ListenPort = 36728,

    [Parameter(Position=7, Mandatory=$true)]
    [System.String]
    $DeploymentKey,

    [Parameter(Position=8, Mandatory=$true)]
    [System.String]
    $BucketName,

    [Parameter(Position=9, Mandatory=$true)]
    [System.String]
    $SMSForwardUser
)

begin {
    Load-AWSCredentials
    $region = Get-AWSRegionFromMetadata

    $workingDirectory = "c:\Users\Public\EWSPushNotifications"
    $command = "$workingDirectory\Start-EWSPushNotifications.ps1"

    function RetreiveUser{
        param(
            [parameter(Mandatory)]
            $Mailboxes,
    
            [parameter(Mandatory)]
            $User
        )
    
        foreach ($mailbox in $Mailboxes) {
            if ($mailbox -and $mailbox.username -and $mailbox.password) {
                $domain, $username = $mailbox.username.split('\\')
                if ($username -eq $User) {
                    return @{
                        'domain'=$domain;
                        'username'=$username;
                        'password'=$mailbox.password
                    }
                }
            }
        }
    }

    $rights = @(
        'SeBatchLogonRight'
    )

    $args = @(
        '-Command', $command,
        '-DomainDNSName', $DomainDNSName,
        '-MailboxUser', $MailboxUser,
        '-Folder', $Folder,
        '-Events', $Events,
        '-LambdaFunction', $LambdaFunction,
        '-ListenPort', $ListenPort,
        '-Region', $region
    )
    $Sta = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -Argument "$args" -WorkingDirectory $workingDirectory 
    $Stt = New-ScheduledTaskTrigger -AtStartup
    $Sts = New-ScheduledTaskSettingsSet -DontStopOnIdleEnd
    $Sts.ExecutionTimeLimit = "PT0S"
}

process {
    foreach ($right in $rights) {
        Grant-UserRight -Account $SMSForwardUser -Right $right
    }

    $kmsKey = Get-KMSKey -KeyId alias/$DeploymentKey | Select-Object "KeyId"
    $kmsKeyId = $kmsKey.KeyId
    $bucketKey = "deploy/$DeploymentKey/.credentials/exchange-mailboxes.json"
    $filePath = "$env:TEMP\exchange-mailboxes.json"

    Read-S3Object -BucketName $BucketName -Key $bucketKey -File $filePath
    $decryptedOutputStream = Invoke-KMSDecrypt -CiphertextBlob $(ConvertFrom-Base64toMemoryStream -Base64Input $(Get-Content $filePath))
    $decryptedOutput = ConvertFrom-StreamToString -inputStream $decryptedOutputStream.Plaintext

    $mailboxes = $decryptedOutput | ConvertFrom-Json
    $user = RetreiveUser -Mailboxes $mailboxes -User $SMSForwardUser
    Remove-Item $filePath

    if ($user.domain -and $user.username -and $user.password) {
        $userName = "$DomainNetBIOSName\$SMSForwardUser"
        $unsecurePassword = $user.password
        $securePassword = ConvertTo-SecureString $unsecurePassword -AsPlainText -Force
        $credentials = New-Object System.Management.Automation.PSCredential ($SMSForwardUser, $securePassword)
        $password = $credentials.GetNetworkCredential().Password

        Register-ScheduledTask -TaskName "RunEWSPushNotifications" -User $userName -Password $password -Action $Sta -Trigger $Stt -Settings $Sts -AsJob -Force

        #$taskFile = "C:\Windows\System32\Tasks\RunEWSPushNotifications"
        #while (!(Test-Path $taskFile)) { Start-Sleep 5 }

        #$Acl = Get-Acl $taskFile
        #$Ar = New-Object System.Security.AccessControl.FileSystemAccessRule($userName, "ReadAndExecute", "Allow")
        #$Acl.SetAccessRule($Ar)
        #Set-Acl $taskFile $Acl
    }
}