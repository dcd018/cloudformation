[CmdletBinding()]
param()

begin {
    $libPath = "$PSScriptRoot\lib"
    $sourcePath = "$libPath\Microsoft.Exchange.WebServices.2.2\lib\40"
    $destPath = "C:\Program Files\Microsoft\Exchange\Web Services\2.2"
}

process {
    cd $libPath
    & "$libPath\nuget.exe" "--%" "install Microsoft.Exchange.WebServices"
    New-Item -ItemType directory -Path $destPath
    Copy-Item -Path "$sourcePath\*" $destPath -Recurse
}