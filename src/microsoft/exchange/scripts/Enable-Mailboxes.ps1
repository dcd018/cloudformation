[CmdletBinding()]
param(
	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainAdminUser,

	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainAdminPassword,
	
	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$DomainDNSName,

	[Parameter(Position=0, Mandatory=$true)]
	[System.String]
	$ADServerNetBIOSName,

	[Parameter(Position=0, Mandatory=$true)]
    [System.String]
	$SMSDefaultUser,
	
	[Parameter(Position=0, Mandatory=$true)]
    [System.String]
    $SMSForwardUser
)

begin {
	$libPath 		  = "$PSScriptRoot\lib\ExchangeManagementConsole"
	$users 			  = Import-Csv $PSScriptRoot\users.csv
	$domainController = "$ADServerNetBIOSName.$DomainDNSName".ToLower()
	
	$args = @(
		'--host', 'exch1', 
		'--domain', $DomainDNSName,
		'--user', $DomainAdminUser,
		'--password', $DomainAdminPassword
	)
}

process {
	foreach ($user in $users) {
		$identity = $user.alias
		$enableMailbox = @(
			'Enable-Mailbox',
			'-Identity', $identity,
			'-DomainController', $domainController
		)
		& "$libPath\ExchangeManagementConsole.exe" "--%" "$args --cmd `"$enableMailbox`""
	}

	$setMailbox = @(
		'Set-Mailbox',
		'-Identity', $SMSDefaultUser,
		'-DomainController', $domainController,
		'-GrantSendOnBehalfTo', $SMSForwardUser
	)
	& "$libPath\ExchangeManagementConsole.exe" "--%" "$args --cmd `"$setMailbox`""
}