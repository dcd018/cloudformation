{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Parameters": {
    "DeploymentKey": {
      "Description": "Deployment key used across AWS resources",
      "Type": "String"
    },
    "ClientName": {
      "Description": "Client Application Name",
      "Type": "String",
      "Default": "CloudAccess"
    },
    "MfaConfiguration": {
      "Description": "MFA Configuration",
      "Type": "String",
      "Default": "OFF"
    },
    "BucketName": {
      "Description": "Bucket name",
      "Type": "String",
      "Default": "example"
    }
  },
  "Resources": {
    "UserPoolSMSRole": {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "AssumeRolePolicyDocument": {
          "Statement": [
            {
              "Sid": "",
              "Effect": "Allow",
              "Principal": {
                "Service": "cognito-idp.amazonaws.com"
              },
              "Action": "sts:AssumeRole",
              "Condition": {
                "StringEquals": {
                  "sts:ExternalId": {
                    "Ref": "DeploymentKey"
                  }
                }
              }
            }
          ]
        },
        "Policies": [
          {
            "PolicyName": {
              "Fn::Sub": "${DeploymentKey}-cognito-${ClientName}-sms"
            },
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Action": [
                    "sns:publish"
                  ],
                  "Resource": [
                    "*"
                  ]
                }
              ]
            }
          }
        ],
        "RoleName": {
          "Fn::Sub": "${DeploymentKey}-cognito-${ClientName}-sms"
        }
      }
    },
    "UserPool": {
      "Type": "AWS::Cognito::UserPool",
      "Properties": {
        "AdminCreateUserConfig": {
          "AllowAdminCreateUserOnly": true,
          "UnusedAccountValidityDays": 1
        },
        "AliasAttributes": [
          "email"
        ],
        "AutoVerifiedAttributes": [
          "email",
          "phone_number"
        ],
        "MfaConfiguration": {
          "Ref": "MfaConfiguration"
        },
        "SmsConfiguration": {
          "ExternalId": {
            "Ref": "DeploymentKey"
          },
          "SnsCallerArn": {
            "Fn::GetAtt": [
              "UserPoolSMSRole",
              "Arn"
            ]
          }
        },
        "UserPoolName": {
          "Ref": "ClientName"
        }
      }
    },
    "UserPoolClient": {
      "Type": "AWS::Cognito::UserPoolClient",
      "Properties": {
        "ClientName": {
          "Ref": "ClientName"
        },
        "GenerateSecret": false,
        "RefreshTokenValidity": 1,
        "UserPoolId": {
          "Ref": "UserPool"
        }
      }
    },
    "IdentityPool": {
      "Type": "AWS::Cognito::IdentityPool",
      "Properties": {
        "IdentityPoolName": {
          "Ref": "ClientName"
        },
        "AllowUnauthenticatedIdentities": false,
        "CognitoIdentityProviders": [
          {
            "ClientId": {
              "Ref": "UserPoolClient"
            },
            "ProviderName": {
              "Fn::Sub": "cognito-idp.${AWS::Region}.amazonaws.com/${UserPool}"
            }
          }
        ]
      }
    },
    "IdentityPoolAuthenticatedRole": {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "AssumeRolePolicyDocument": {
          "Statement": [
            {
              "Effect": "Allow",
              "Principal": {
                "Federated": "cognito-identity.amazonaws.com"
              },
              "Action": "sts:AssumeRoleWithWebIdentity",
              "Condition": {
                "StringEquals": {
                  "cognito-identity.amazonaws.com:aud": {
                    "Ref": "IdentityPool"
                  }
                },
                "ForAnyValue:StringLike": {
                  "cognito-identity.amazonaws.com:amr": "authenticated"
                }
              }
            }
          ]
        },
        "Policies": [
          {
            "PolicyName": {
              "Fn::Sub": "${DeploymentKey}-${ClientName}-authenticated"
            },
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Action": [
                    "mobileanalytics:PutEvents",
                    "cognito-sync:*",
                    "cognito-identity:*"
                  ],
                  "Resource": [
                    "*"
                  ]
                },
                {
                  "Action": [
                    "s3:ListBucket"
                  ],
                  "Resource": [
                    {
                      "Fn::Sub": "arn:aws:s3:::${BucketName}"
                    }
                  ],
                  "Effect": "Allow"
                },
                {
                  "Action": [
                    "s3:GetObject*"
                  ],
                  "Resource": [
                    {
                      "Fn::Sub": "arn:aws:s3:::${BucketName}/deploy/${DeploymentKey}/.credentials/*"
                    }
                  ],
                  "Effect": "Allow"
                },
                {
                  "Action": [
                    "kms:Encrypt",
                    "kms:Decrypt",
                    "kms:ReEncrypt*",
                    "kms:GenerateDataKey*",
                    "kms:DescribeKey"
                  ],
                  "Resource": [
                    {
                      "Fn::Sub": "arn:aws:kms:${AWS::Region}:${AWS::AccountId}:alias/${DeploymentKey}"
                    }
                  ],
                  "Effect": "Allow"
                },
                {
                  "Action": [
                    "ec2:*"
                  ],
                  "Resource": [
                    "*"
                  ],
                  "Effect": "Allow"
                }
              ]
            }
          }
        ],
        "RoleName": {
          "Fn::Sub": "${DeploymentKey}-${ClientName}-authenticated"
        }
      }
    },
    "IdentityPoolUnauthenticatedRole": {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "AssumeRolePolicyDocument": {
          "Statement": [
            {
              "Effect": "Allow",
              "Principal": {
                "Federated": "cognito-identity.amazonaws.com"
              },
              "Action": "sts:AssumeRoleWithWebIdentity",
              "Condition": {
                "StringEquals": {
                  "cognito-identity.amazonaws.com:aud": {
                    "Ref": "IdentityPool"
                  }
                },
                "ForAnyValue:StringLike": {
                  "cognito-identity.amazonaws.com:amr": "unauthenticated"
                }
              }
            }
          ]
        },
        "Policies": [
          {
            "PolicyName": {
              "Fn::Sub": "${DeploymentKey}-${ClientName}-unauthenticated"
            },
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Action": [
                    "mobileanalytics:PutEvents",
                    "cognito-sync:*"
                  ],
                  "Resource": [
                    "*"
                  ]
                }
              ]
            }
          }
        ],
        "RoleName": {
          "Fn::Sub": "${DeploymentKey}-${ClientName}-unauthenticated"
        }
      }
    },
    "IdentityPoolRoleAttachment": {
      "Type": "AWS::Cognito::IdentityPoolRoleAttachment",
      "Properties": {
        "IdentityPoolId": {
          "Ref": "IdentityPool"
        },
        "Roles": {
          "authenticated": {
            "Fn::GetAtt": [
              "IdentityPoolAuthenticatedRole",
              "Arn"
            ]
          },
          "unauthenticated": {
            "Fn::GetAtt": [
              "IdentityPoolUnauthenticatedRole",
              "Arn"
            ]
          }
        }
      }
    }
  }
}