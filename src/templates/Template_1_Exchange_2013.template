{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Description": "(000E) - This template deploys two Exchange Server 2013 servers. This template is intended to be installed into an existing VPC that was built using the sample reference architecture titled: \"Implementing Active Directory Domain Services in the AWS Cloud\" **WARNING** This template creates Amazon EC2 Windows instance and related resources. You will be billed for the AWS resources used if you create a stack from this template.",
    "Parameters": {
        "KeyPairName": {
            "Description": "Public/private key pairs allow you to securely connect to your instance after it launches",
            "Type": "AWS::EC2::KeyPair::KeyName"
        },
        "EXCHInstanceType": {
            "Description": "Amazon EC2 instance type for the Exchange Servers",
            "Type": "String",
            "Default": "r3.xlarge",
            "AllowedValues": [
                "t2.micro",
                "t2.small",
                "t2.medium",
                "r3.xlarge",
                "r3.2xlarge",
                "r3.4xlarge"
            ],
            "ConstraintDescription": "Only EBS Optimized instance types r3.xlarge, r3.2xlarge, r3.4xlarge allowed"
        },
        "DomainDNSName": {
            "Description": "Fully qualified domain name (FQDN) of the forest root domain e.g. corp.example.com",
            "Type": "String",
            "Default": "example.com",
            "MinLength": "3",
            "MaxLength": "25",
            "AllowedPattern": "[a-zA-Z0-9\\-]+\\..+"
        },
        "DomainNetBIOSName": {
            "Description": "NetBIOS name of the domain (upto 15 characters) for users of earlier versions of Windows e.g. CORP",
            "Type": "String",
            "Default": "example",
            "MinLength": "1",
            "MaxLength": "15",
            "AllowedPattern": "[a-zA-Z0-9\\-]+"
        },
        "ADServerNetBIOSName1": {
            "Description": "NetBIOS name of the existing Domain Controller in AZ1",
            "Type": "String",
            "Default": "DC1",
            "MinLength": "1",
            "MaxLength": "15",
            "AllowedPattern": "[a-zA-Z0-9\\-]+"
        },
        "DomainAdminUser": {
            "Description": "User name for the account that will be added as Domain Administrator. This is separate from the default \"Administrator\" account",
            "Type": "String",
            "Default": "StackAdmin",
            "MinLength": "5",
            "MaxLength": "25",
            "AllowedPattern": "[a-zA-Z0-9]*"
        },
        "DomainAdminPassword": {
            "Description": "Password for the domain admin user. Must be at least 8 characters containing letters, numbers and symbols",
            "Type": "String",
            "MinLength": "8",
            "MaxLength": "32",
            "AllowedPattern": "(?=^.{6,255}$)((?=.*\\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))^.*",
            "NoEcho": "true"
        },
        "ADServer1PrivateIp": {
            "Description": "Fixed private IP for the first existing Active Directory server located in AZ1",
            "Type": "String",
            "Default": "10.0.0.10"
        },
        "EXCH1PrivateIp": {
            "Description": "Primary private IP for the first Exchange Server",
            "Type": "String",
            "Default": "10.0.0.150"
        },
        "EXCH1PrivateIp2": {
            "Description": "Secondary private IP for the first Exchange Server",
            "Type": "String",
            "Default": "10.0.0.151"
        },
        "DomainMemberSGID": {
            "Description": "ID of the Domain Member Security Group (e.g., sg-7f16e910)",
            "Type": "AWS::EC2::SecurityGroup::Id"
        },
        "VPC": {
            "Description": "ID of the VPC (e.g., vpc-0343606e)",
            "Type": "AWS::EC2::VPC::Id"
        },
        "PrivateSubnet1Id": {
            "Description": "ID of the subnet you want to provision the first Exchange Server into (e.g., subnet-a0246dcd)",
            "Type": "AWS::EC2::Subnet::Id"
        },
        "PublicSubnet1Id": {
            "Description": "ID of the public subnet you want to provision the first Edge Server into (e.g., subnet-a0246dcd)",
            "Type": "AWS::EC2::Subnet::Id"
        },
        "PrivSub1CIDR": {
            "Description": "CIDR Block for Private Subnet located in AZ1",
            "Type": "String",
            "Default": "10.0.0.0/19",
            "AllowedPattern": "[a-zA-Z0-9]+\\..+"
        },
        "VPCCIDR": {
            "Description": "CIDR Block used by the VPC",
            "Type": "String",
            "Default": "10.0.0.0/16",
            "AllowedPattern": "[a-zA-Z0-9]+\\..+"
        }
    },
    "Mappings": {
        "AWSAMIRegionMap": {
            "AMI": {
                "WS2012R2": "Windows_Server-2012-R2_RTM-English-64Bit-Base-2017.09.13"
            },
            "ap-northeast-1": {
                "WS2012R2": "ami-ca5b99ac"
            },
            "ap-northeast-2": {
                "WS2012R2": "ami-1c07dc72"
            },
            "ap-south-1": {
                "WS2012R2": "ami-a4f3b4cb"
            },
            "ap-southeast-1": {
                "WS2012R2": "ami-440b7e27"
            },
            "ap-southeast-2": {
                "WS2012R2": "ami-ed33d48f"
            },
            "eu-central-1": {
                "WS2012R2": "ami-9500b7fa"
            },
            "eu-west-1": {
                "WS2012R2": "ami-4a18db33"
            },
            "sa-east-1": {
                "WS2012R2": "ami-e9344985"
            },
            "us-east-1": {
                "WS2012R2": "ami-23d93c59"
            },
            "us-east-2": {
                "WS2012R2": "ami-f9e1c39c"
            },
            "us-west-1": {
                "WS2012R2": "ami-794a7c19"
            },
            "us-west-2": {
                "WS2012R2": "ami-9d9360e5"
            }
        }
    },
    "Resources": {
        "EXCH1WaitCondition": {
            "Type": "AWS::CloudFormation::WaitCondition",
            "DependsOn": "EXCH1",
            "Properties": {
                "Handle": {
                    "Ref": "EXCH1WaitHandle"
                },
                "Timeout": "5400"
            }
        },
        "EXCH1WaitHandle": {
            "Type": "AWS::CloudFormation::WaitConditionHandle"
        },
        "EXCH1": {
            "Type": "AWS::EC2::Instance",
            "Metadata": {
                "AWS::CloudFormation::Init": {
                    "configSets": {
                        "config": [
                            "setup",
                            "rename",
                            "join",
                            "installexch",
                            "finalize"
                        ]
                    },
                    "setup": {
                        "files": {
                            "c:\\cfn\\cfn-hup.conf": {
                                "content": {
                                    "Fn::Join": [
                                        "",
                                        [
                                            "[main]\n",
                                            "stack=",
                                            {
                                                "Ref": "AWS::StackId"
                                            },
                                            "\n",
                                            "region=",
                                            {
                                                "Ref": "AWS::Region"
                                            },
                                            "\n"
                                        ]
                                    ]
                                }
                            },
                            "c:\\cfn\\hooks.d\\cfn-auto-reloader.conf": {
                                "content": {
                                    "Fn::Join": [
                                        "",
                                        [
                                            "[cfn-auto-reloader-hook]\n",
                                            "triggers=post.update\n",
                                            "path=Resources.EXCH1.Metadata.AWS::CloudFormation::Init\n",
                                            "action=cfn-init.exe -v -s ",
                                            {
                                                "Ref": "AWS::StackId"
                                            },
                                            " -r EXCH1",
                                            " --region ",
                                            {
                                                "Ref": "AWS::Region"
                                            },
                                            "\n"
                                        ]
                                    ]
                                }
                            },
                            "c:\\cfn\\scripts\\Install.bat": {
                                "content": {
                                    "Fn::Join": [
                                        "",
                                        [
                                            "powershell.exe -command c:\\cfn\\scripts\\Install-UcmaRuntime.ps1\n",
                                            "powershell.exe -command c:\\cfn\\scripts\\Install-Exch2013Org.ps1 -InstallPath \\\\",
                                            {
                                                "Ref": "ADServerNetBIOSName1"
                                            },
                                            "\\exchinstall",
                                            "\n",
                                            "powershell.exe -command c:\\cfn\\scripts\\Install-Exch2013.ps1 -InstallPath \\\\",
                                            {
                                                "Ref": "ADServerNetBIOSName1"
                                            },
                                            "\\exchinstall ",
                                            "-Server 1",
                                            "\n"
                                        ]
                                    ]
                                }
                            },
                            "c:\\cfn\\scripts\\Unzip-Archive.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/submodules/quickstart-microsoft-utilities/scripts/Unzip-Archive.ps1"
                            },
                            "c:\\cfn\\scripts\\Initialize-ExchangeVolume.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/scripts/Initialize-ExchangeVolume.ps1"
                            },
                            "c:\\cfn\\modules\\AWSQuickStart.zip": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/submodules/quickstart-microsoft-utilities/modules/AWSQuickStart.zip"
                            },
                            "c:\\cfn\\scripts\\CreateWaitHandle.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/submodules/quickstart-microsoft-utilities/scripts/CreateWaitHandle.ps1"
                            },
                            "c:\\cfn\\scripts\\Create-Share.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/scripts/Create-Share.ps1"
                            },
                            "c:\\cfn\\scripts\\Create-Folder.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/scripts/Create-Folder.ps1"
                            },
                            "c:\\cfn\\scripts\\Install-ExchPreReq.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/scripts/Install-ExchPreReq.ps1"
                            },
                            "c:\\cfn\\scripts\\Download-File.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/submodules/quickstart-microsoft-utilities/scripts/Download-File.ps1"
                            },
                            "c:\\cfn\\scripts\\Install-UcmaRuntime.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/scripts/Install-UcmaRuntime.ps1"
                            },
                            "c:\\cfn\\downloads\\UcmaRuntimeSetup.exe": {
                                "source": "http://download.microsoft.com/download/2/C/4/2C47A5C1-A1F3-4843-B9FE-84C0032C61EC/UcmaRuntimeSetup.exe"
                            },
                            "c:\\cfn\\scripts\\Join-Domain.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/submodules/quickstart-microsoft-utilities/scripts/Join-Domain.ps1"
                            },
                            "c:\\cfn\\scripts\\Install-Exch2013Org.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/scripts/Install-Exch2013Org.ps1"
                            },
                            "c:\\cfn\\scripts\\Install-Exch2013.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/scripts/Install-Exch2013.ps1"
                            },
                            "c:\\cfn\\scripts\\Expand-ExchangeFiles.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/scripts/Expand-ExchangeFiles.ps1"
                            },
                            "c:\\cfn\\scripts\\Disable-AutoLogon.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/scripts/Disable-AutoLogon.ps1"
                            },
                            "c:\\cfn\\scripts\\Enable-AutoLogon.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/scripts/Enable-AutoLogon.ps1"
                            },
                            "c:\\cfn\\scripts\\Invoke-ADReplication.ps1": {
                                "source": "https://s3.amazonaws.com/quickstart-reference/microsoft/exchange/latest/submodules/quickstart-microsoft-utilities/scripts/Invoke-ADReplication.ps1"
                            }
                        },
                        "commands": {
                            "a-set-execution-policy": {
                                "command": "powershell.exe -command Set-ExecutionPolicy RemoteSigned -Force",
                                "waitAfterCompletion": "0"
                            },
                            "b-unpack-quickstart-module": {
                                "command": "powershell.exe -command c:\\cfn\\scripts\\Unzip-Archive.ps1 -Source c:\\cfn\\modules\\AWSQuickStart.zip -Destination C:\\Windows\\system32\\WindowsPowerShell\\v1.0\\Modules\\",
                                "waitAfterCompletion": "0"
                            },
                            "c-create-waithandle": {
                                "command": {
                                    "Fn::Join": [
                                        "",
                                        [
                                            "powershell.exe -command \"c:\\cfn\\scripts\\CreateWaitHandle.ps1 -Handle '",
                                            {
                                                "Ref": "EXCH1WaitHandle"
                                            },
                                            "'\""
                                        ]
                                    ]
                                },
                                "waitAfterCompletion": "0"
                            },
                            "d-initialize-db-volume": {
                                "command": "powershell.exe -command c:\\cfn\\scripts\\Initialize-ExchangeVolume.ps1 -DriveLetter d",
                                "waitAfterCompletion": "0"
                            }
                        },
                        "services": {
                            "windows": {
                                "cfn-hup": {
                                    "enabled": "true",
                                    "ensureRunning": "true",
                                    "files": [
                                        "c:\\cfn\\cfn-hup.conf",
                                        "c:\\cfn\\hooks.d\\cfn-auto-reloader.conf"
                                    ]
                                }
                            }
                        }
                    },
                    "rename": {
                        "commands": {
                            "a-execute-powershell-script-RenameComputer": {
                                "command": "powershell.exe -Command Rename-Computer -NewName EXCH1 -Restart",
                                "waitAfterCompletion": "forever"
                            }
                        }
                    },
                    "join": {
                        "commands": {
                            "a-set-dns-servers": {
                                "command": {
                                    "Fn::Join": [
                                        "",
                                        [
                                            "powershell.exe -Command ",
                                            "\"",
                                            "Get-NetAdapter | Set-DnsClientServerAddress -ServerAddresses ",
                                            {
                                                "Ref": "ADServer1PrivateIp"
                                            },
                                            "\""
                                        ]
                                    ]
                                },
                                "waitAfterCompletion": "30"
                            },
                            "b-join-domain": {
                                "command": {
                                    "Fn::Join": [
                                        "",
                                        [
                                            "powershell.exe -Command c:\\cfn\\scripts\\Join-Domain.ps1 -DomainName ",
                                            {
                                                "Ref": "DomainDNSName"
                                            },
                                            " -UserName ",
                                            {
                                                "Ref": "DomainNetBIOSName"
                                            },
                                            "\\",
                                            {
                                                "Ref": "DomainAdminUser"
                                            },
                                            " -Password ",
                                            {
                                                "Ref": "DomainAdminPassword"
                                            }
                                        ]
                                    ]
                                },
                                "waitAfterCompletion": "forever"
                            }
                        }
                    },
                    "installexch": {
                        "commands": {
                            "a-create-folder": {
                                "command": {
                                    "Fn::Join": [
                                        "",
                                        [
                                            "powershell.exe -Command c:\\cfn\\scripts\\Create-Folder.ps1 -ComputerName ",
                                            {
                                                "Ref": "ADServerNetBIOSName1"
                                            },
                                            " -UserName ",
                                            {
                                                "Ref": "DomainNetBIOSName"
                                            },
                                            "\\",
                                            {
                                                "Ref": "DomainAdminUser"
                                            },
                                            " -Password ",
                                            {
                                                "Ref": "DomainAdminPassword"
                                            },
                                            " -FolderName exchinstall"
                                        ]
                                    ]
                                },
                                "waitAfterCompletion": "0"
                            },
                            "b-create-share": {
                                "command": {
                                    "Fn::Join": [
                                        "",
                                        [
                                            "powershell.exe -Command c:\\cfn\\scripts\\Create-Share.ps1 -ComputerName ",
                                            {
                                                "Ref": "ADServerNetBIOSName1"
                                            },
                                            " -UserName ",
                                            {
                                                "Ref": "DomainNetBIOSName"
                                            },
                                            "\\",
                                            {
                                                "Ref": "DomainAdminUser"
                                            },
                                            " -Password ",
                                            {
                                                "Ref": "DomainAdminPassword"
                                            },
                                            " -FolderName c:\\exchinstall",
                                            " -ShareName exchinstall"
                                        ]
                                    ]
                                },
                                "waitAfterCompletion": "0"
                            },
                            "c-download-exch": {
                                "command": {
                                    "Fn::Join": [
                                        "",
                                        [
                                            "powershell.exe -Command c:\\cfn\\scripts\\Download-File.ps1 -Source https://download.microsoft.com/download/3/A/5/3A5CE1A3-FEAA-4185-9A27-32EA90831867/Exchange2013-x64-cu15.exe",
                                            " -Destination \\\\",
                                            {
                                                "Ref": "ADServerNetBIOSName1"
                                            },
                                            "\\exchinstall\\Exchange2013-x64.exe"
                                        ]
                                    ]
                                },
                                "waitAfterCompletion": "0"
                            },
                            "d-expand-exch": {
                                "command": {
                                    "Fn::Join": [
                                        "",
                                        [
                                            "powershell.exe -Command c:\\cfn\\scripts\\Expand-ExchangeFiles.ps1 -ComputerName ",
                                            {
                                                "Ref": "ADServerNetBIOSName1"
                                            },
                                            " -UserName ",
                                            {
                                                "Ref": "DomainNetBIOSName"
                                            },
                                            "\\",
                                            {
                                                "Ref": "DomainAdminUser"
                                            },
                                            " -Password ",
                                            {
                                                "Ref": "DomainAdminPassword"
                                            }
                                        ]
                                    ]
                                },
                                "waitAfterCompletion": "0"
                            },
                            "e-install-prereq": {
                                "command": "powershell.exe -Command c:\\cfn\\scripts\\Install-ExchPreReq.ps1",
                                "waitAfterCompletion": "forever"
                            },
                            "f-enable-autologon": {
                                "command": {
                                    "Fn::Join": [
                                        "",
                                        [
                                            "powershell.exe -Command c:\\cfn\\scripts\\Enable-AutoLogon.ps1",
                                            " -UserName ",
                                            {
                                                "Ref": "DomainNetBIOSName"
                                            },
                                            "\\",
                                            {
                                                "Ref": "DomainAdminUser"
                                            },
                                            " -Password ",
                                            {
                                                "Ref": "DomainAdminPassword"
                                            },
                                            " -StartupScript c:\\cfn\\scripts\\Install.bat"
                                        ]
                                    ]
                                },
                                "waitAfterCompletion": "0"
                            },
                            "g-reboot": {
                                "command": "powershell.exe -command Restart-Computer -Force",
                                "waitAfterCompletion": "forever"
                            },
                            "h-wait": {
                                "command": "powershell.exe -command Start-Sleep -Seconds 300",
                                "waitAfterCompletion": "0"
                            },
                            "i-invoke-ad-replication": {
                                "command": {
                                    "Fn::Join": [
                                        "",
                                        [
                                            "powershell.exe -Command c:\\cfn\\scripts\\Invoke-ADReplication.ps1",
                                            " -UserName ",
                                            {
                                                "Ref": "DomainNetBIOSName"
                                            },
                                            "\\",
                                            {
                                                "Ref": "DomainAdminUser"
                                            },
                                            " -Password ",
                                            {
                                                "Ref": "DomainAdminPassword"
                                            },
                                            " -DomainController ",
                                            {
                                                "Ref": "ADServerNetBIOSName1"
                                            }
                                        ]
                                    ]
                                },
                                "waitAfterCompletion": "0"
                            },
                            "j-disable-autologon": {
                                "command": "powershell.exe -command c:\\cfn\\scripts\\Disable-AutoLogon.ps1",
                                "waitAfterCompletion": "0"
                            }
                        }
                    },
                    "finalize": {
                        "commands": {
                            "a-write-status": {
                                "command": "powershell.exe -command Write-AWSQuickStartStatus",
                                "waitAfterCompletion": "0"
                            }
                        }
                    }
                }
            },
            "Properties": {
                "ImageId": {
                    "Fn::FindInMap": [
                        "AWSAMIRegionMap",
                        {
                            "Ref": "AWS::Region"
                        },
                        "WS2012R2"
                    ]
                },
                "InstanceType": {
                    "Ref": "EXCHInstanceType"
                },
                "NetworkInterfaces": [
                    {
                        "DeleteOnTermination": "true",
                        "DeviceIndex": 0,
                        "SubnetId": {
                            "Ref": "PrivateSubnet1Id"
                        },
                        "PrivateIpAddresses": [
                            {
                                "Primary": "true",
                                "PrivateIpAddress": {
                                    "Ref": "EXCH1PrivateIp"
                                }
                            },
                            {
                                "Primary": "false",
                                "PrivateIpAddress": {
                                    "Ref": "EXCH1PrivateIp2"
                                }
                            }
                        ],
                        "GroupSet": [
                            {
                                "Ref": "DomainMemberSGID"
                            },
                            {
                                "Ref": "EXCHClientSecurityGroup"
                            }
                        ]
                    }
                ],
                "Tags": [
                    {
                        "Key": "Name",
                        "Value": "EXCH1"
                    }
                ],
                "BlockDeviceMappings": [
                    {
                        "DeviceName": "/dev/sda1",
                        "Ebs": {
                            "VolumeSize": "300",
                            "VolumeType": "gp2"
                        }
                    },
                    {
                        "DeviceName": "/dev/xvdb",
                        "VirtualName": "ephemeral0"
                    }
                ],
                "Volumes": [
                    {
                        "VolumeId": {
                            "Ref": "EXCH1Volume1"
                        },
                        "Device": "/dev/xvdf"
                    }
                ],
                "KeyName": {
                    "Ref": "KeyPairName"
                },
                "UserData": {
                    "Fn::Base64": {
                        "Fn::Join": [
                            "",
                            [
                                "<script>\n",
                                "cfn-init.exe -v -c config -s ",
                                {
                                    "Ref": "AWS::StackId"
                                },
                                " -r EXCH1 ",
                                " --region ",
                                {
                                    "Ref": "AWS::Region"
                                },
                                "\n",
                                "</script>"
                            ]
                        ]
                    }
                }
            }
        },
        "EXCH1Volume1": {
            "Type": "AWS::EC2::Volume",
            "Properties": {
                "Size": "1000",
                "VolumeType": "gp2",
                "AvailabilityZone": {
                    "Fn::Select": [
                        0,
                        {
                            "Fn::GetAZs": ""
                        }
                    ]
                }
            }
        },
        "EXCHClientSecurityGroup": {
            "Type": "AWS::EC2::SecurityGroup",
            "Properties": {
                "GroupDescription": "Enable communications from clients to Exchange Servers",
                "VpcId": {
                    "Ref": "VPC"
                },
                "SecurityGroupIngress": [
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "443",
                        "ToPort": "443",
                        "CidrIp": {
                            "Ref": "VPCCIDR"
                        }
                    },
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "80",
                        "ToPort": "80",
                        "CidrIp": {
                            "Ref": "VPCCIDR"
                        }
                    },
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "143",
                        "ToPort": "143",
                        "CidrIp": {
                            "Ref": "VPCCIDR"
                        }
                    },
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "993",
                        "ToPort": "993",
                        "CidrIp": {
                            "Ref": "VPCCIDR"
                        }
                    },
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "110",
                        "ToPort": "110",
                        "CidrIp": {
                            "Ref": "VPCCIDR"
                        }
                    },
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "995",
                        "ToPort": "995",
                        "CidrIp": {
                            "Ref": "VPCCIDR"
                        }
                    },
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "25",
                        "ToPort": "25",
                        "CidrIp": {
                            "Ref": "VPCCIDR"
                        }
                    },
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "587",
                        "ToPort": "587",
                        "CidrIp": {
                            "Ref": "VPCCIDR"
                        }
                    },
                    {
                        "IpProtocol": "tcp",
                        "FromPort": "5075",
                        "ToPort": "5077",
                        "CidrIp": {
                            "Ref": "VPCCIDR"
                        }
                    }
                ]
            }
        }
    }
}