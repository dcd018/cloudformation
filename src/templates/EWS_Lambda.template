{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Parameters": {
    "DeploymentKey": {
      "Description": "Deployment key used across AWS resources",
      "Type": "String"
    },
    "VPCID": {
      "Description": "ID of the VPC (e.g., vpc-0343606e)",
      "Type": "AWS::EC2::VPC::Id"
    },
    "NATGatewaySGID": {
      "Description": "ID of the NAT Gateway Security Group (e.g., sg-7f16e910)",
      "Type": "AWS::EC2::SecurityGroup::Id"
    },
    "EXCHClientSGID": {
      "Description": "ID of the Exchange Client Security Group (e.g., sg-7f16e910)",
      "Type": "AWS::EC2::SecurityGroup::Id"
    },
    "EXCHServerPrivateIp": {
      "Description": "Primary private IP of the Exchange Server",
      "Type": "String",
      "Default": "10.0.0.150"
    },
    "PrivateSubnetID": {
      "Description": "ID of the private subnet the Exchange Server resides in.",
      "Type": "AWS::EC2::Subnet::Id"
    },
    "BucketName": {
      "Description": "Bucket name for Cloudformation StackSet templates and deployment scripts",
      "Type": "String",
      "Default": "example"
    },
    "BucketMediaKey": {
      "Description": "Bucket key for SMS media",
      "Type": "String",
      "Default": "media"
    },
    "BucketLambdaKey": {
      "Description": "Bucket key for Lambda packages",
      "Type": "String",
      "Default": "lambda"
    },
    "DomainDNSName": {
      "Description": "Fully qualified domain name (FQDN) of the forest root domain e.g. example.com",
      "Type": "String",
      "Default": "example.com"
    },
    "SMSDefaultUser": {
      "Description": "Default SMS user.",
      "Type": "String",
      "Default": "admin"
    },
    "SMSForwardUser": {
      "Description": "User that handles SMS forwarding.",
      "Type": "String",
      "Default": "sms"
    },
    "SMSForwardUserPassword": {
      "Type": "String",
      "NoEcho": "true"
    },
    "TwilioAccountSid": {
      "Description": "Twilio Account SID",
      "Type": "String"
    },
    "TwilioAuthToken": {
      "Description": "Twilio Auth Token",
      "Type": "String",
      "NoEcho": "true"
    },
    "TwilioNumber": {
      "Description": "Twilio number",
      "Type": "String"
    },
    "TwilioUserAgent": {
      "Description": "Twilio user agent string",
      "Type": "String",
      "Default": "TwilioProxy/1.1"
    },
    "HealthCheckUserAgent": {
      "Description": "User agent string of health check requests",
      "Type": "String",
      "Default": "HealthCheck/1.1"
    }
  },
  "Resources": {
    "LambdaExecutionRole": {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "AssumeRolePolicyDocument": {
          "Statement": [
            {
              "Effect": "Allow",
              "Principal": {
                "Service": [
                  "lambda.amazonaws.com"
                ]
              },
              "Action": [
                "sts:AssumeRole"
              ]
            }
          ]
        },
        "Policies": [
          {
            "PolicyName": {
              "Fn::Sub": "${DeploymentKey}-lambda"
            },
            "PolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Action": [
                    "logs:CreateLogGroup",
                    "logs:CreateLogStream",
                    "logs:PutLogEvents",
                    "ec2:CreateNetworkInterface",
                    "ec2:DescribeNetworkInterfaces",
                    "ec2:DeleteNetworkInterface"
                  ],
                  "Resource": "*"
                },
                {
                  "Action": [
                    "s3:ListBucket"
                  ],
                  "Resource": [
                    "arn:aws:s3:::intovpc"
                  ],
                  "Effect": "Allow"
                },
                {
                  "Action": [
                    "s3:GetObject*",
                    "s3:PutObject*",
                    "s3:DeleteObject"
                  ],
                  "Resource": [
                    "arn:aws:s3:::intovpc/media/*"
                  ],
                  "Effect": "Allow"
                },
                {
                  "Effect": "Allow",
                  "Action": [
                    "lambda:InvokeFunction"
                  ],
                  "Resource": [
                    {
                      "Fn::Sub": "arn:aws:lambda:${AWS::Region}:${AWS::AccountId}:function:exchangeReceiveConnector"
                    }
                  ]
                }
              ]
            }
          }
        ],
        "RoleName": {
          "Fn::Sub": "${DeploymentKey}-lambda"
        }
      }
    },
    "LambdaEWSSG": {
      "DependsOn": "LambdaExecutionRole",
      "Type": "AWS::EC2::SecurityGroup",
      "Properties": {
        "VpcId": {
          "Ref": "VPCID"
        },
        "GroupName": "Lambda EWS",
        "GroupDescription": "Lambda EWS Security Group",
        "SecurityGroupEgress": [
          {
            "CidrIp": "0.0.0.0/0",
            "ToPort": -1,
            "FromPort": -1,
            "IpProtocol": "-1"
          }
        ],
        "Tags": [
          {
            "Key": "Name",
            "Value": "Lambda EWS"
          }
        ]
      }
    },
    "NATGatewaySGIngressHTTPLambdaEWSSG": {
      "DependsOn": "LambdaEWSSG",
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupId": {
          "Ref": "NATGatewaySGID"
        },
        "IpProtocol": "tcp",
        "FromPort": "80",
        "ToPort": "80",
        "SourceSecurityGroupId": {
          "Ref": "LambdaEWSSG"
        }
      }
    },
    "NATGatewaySGIngressHTTPSLambdaEWSSG": {
      "DependsOn": "LambdaEWSSG",
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupId": {
          "Ref": "NATGatewaySGID"
        },
        "IpProtocol": "tcp",
        "FromPort": "443",
        "ToPort": "443",
        "SourceSecurityGroupId": {
          "Ref": "LambdaEWSSG"
        }
      }
    },
    "NATGatewaySGIngressDNSLambdaEWSSG": {
      "DependsOn": "LambdaEWSSG",
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupId": {
          "Ref": "NATGatewaySGID"
        },
        "IpProtocol": "udp",
        "FromPort": "53",
        "ToPort": "53",
        "SourceSecurityGroupId": {
          "Ref": "LambdaEWSSG"
        }
      }
    },
    "EXCHClientSGIngressHTTPSLambdaEWSSG": {
      "DependsOn": "LambdaEWSSG",
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupId": {
          "Ref": "EXCHClientSGID"
        },
        "IpProtocol": "tcp",
        "FromPort": "443",
        "ToPort": "443",
        "SourceSecurityGroupId": {
          "Ref": "LambdaEWSSG"
        }
      }
    },
    "EXCHClientSGIngressSMTPLambdaEWSSG": {
      "DependsOn": "LambdaEWSSG",
      "Type": "AWS::EC2::SecurityGroupIngress",
      "Properties": {
        "GroupId": {
          "Ref": "EXCHClientSGID"
        },
        "IpProtocol": "tcp",
        "FromPort": "587",
        "ToPort": "587",
        "SourceSecurityGroupId": {
          "Ref": "LambdaEWSSG"
        }
      }
    },
    "ExchangeReceiveConnectorLambdaFunction": {
      "DependsOn": "LambdaEWSSG",
      "Type": "AWS::Lambda::Function",
      "Properties": {
        "Code": {
          "S3Bucket": "deflavia.me",
          "S3Key": {
            "Fn::Sub": "${BucketLambdaKey}/exchangeReceiveConnector.zip"
          }
        },
        "Environment": {
          "Variables": {
            "BUCKET_NAME": {
              "Ref": "BucketName"
            },
            "BUCKET_PREFIX": {
              "Ref": "BucketMediaKey"
            },
            "TMP_DIR": "/tmp",
            "EXCH_USER": {
              "Ref": "SMSForwardUser"
            },
            "EXCH_PASSWORD": {
              "Ref": "SMSForwardUserPassword"
            },
            "EXCH_SMTP_HOST": {
              "Ref": "EXCHServerPrivateIp"
            },
            "EXCH_ENDPOINT": {
              "Fn::Sub": "https://${EXCHServerPrivateIp}/ews/exchange.asmx"
            },
            "EXCH_DOMAIN": {
              "Ref": "DomainDNSName"
            },
            "EXCH_MAILBOX": {
              "Fn::Sub": "${SMSDefaultUser}@${DomainDNSName}"
            },
            "TWILIO_ACCOUNT_SID": {
              "Ref": "TwilioAccountSid"
            },
            "TWILIO_AUTH_TOKEN": {
              "Ref": "TwilioAuthToken"
            },
            "TWILIO_NUMBER": {
              "Ref": "TwilioNumber"
            },
            "TWILIO_USER_AGENT": {
              "Ref": "TwilioUserAgent"
            },
            "HEALTH_CHECK_USER_AGENT": {
              "Ref": "HealthCheckUserAgent"
            }
          }
        },
        "FunctionName": "exchangeReceiveConnector",
        "Handler": "index.handler",
        "MemorySize": 128,
        "Role": {
          "Fn::GetAtt": [
            "LambdaExecutionRole",
            "Arn"
          ]
        },
        "Runtime": "nodejs6.10",
        "Timeout": 300,
        "VpcConfig": {
          "SecurityGroupIds": [
            {
              "Ref": "LambdaEWSSG"
            }
          ],
          "SubnetIds": [
            {
              "Ref": "PrivateSubnetID"
            }
          ]
        }
      }
    },
    "RestApi": {
      "DependsOn": "ExchangeReceiveConnectorLambdaFunction",
      "Type": "AWS::ApiGateway::RestApi",
      "Properties": {
        "Name": "SMS API gateway"
      }
    },
    "TwilioRestApiResource": {
      "DependsOn": "RestApi",
      "Type": "AWS::ApiGateway::Resource",
      "Properties": {
        "ParentId": {
          "Fn::GetAtt": [
            "RestApi",
            "RootResourceId"
          ]
        },
        "PathPart": "twilio",
        "RestApiId": {
          "Ref": "RestApi"
        }
      }
    },
    "TwilioSmsRestApiResource": {
      "DependsOn": "TwilioRestApiResource",
      "Type": "AWS::ApiGateway::Resource",
      "Properties": {
        "ParentId": {
          "Ref": "TwilioRestApiResource"
        },
        "PathPart": "sms",
        "RestApiId": {
          "Ref": "RestApi"
        }
      }
    },
    "TwilioSmsProxyRestApiResource": {
      "DependsOn": "TwilioSmsRestApiResource",
      "Type": "AWS::ApiGateway::Resource",
      "Properties": {
        "ParentId": {
          "Ref": "TwilioSmsRestApiResource"
        },
        "PathPart": "{proxy+}",
        "RestApiId": {
          "Ref": "RestApi"
        }
      }
    },
    "TwilioSmsProxyRestApiMethod": {
      "DependsOn": "TwilioSmsProxyRestApiResource",
      "Type": "AWS::ApiGateway::Method",
      "Properties": {
        "RestApiId": {
          "Ref": "RestApi"
        },
        "ResourceId": {
          "Ref": "TwilioSmsProxyRestApiResource"
        },
        "HttpMethod": "ANY",
        "AuthorizationType": "NONE",
        "Integration": {
          "Type": "AWS_PROXY",
          "IntegrationHttpMethod": "POST",
          "Uri": {
            "Fn::Sub": "arn:aws:apigateway:${AWS::Region}:lambda:path/2015-03-31/functions/${ExchangeReceiveConnectorLambdaFunction.Arn}/invocations"
          }
        }
      }
    },
    "RestApiDeployment": {
      "DependsOn": "TwilioSmsProxyRestApiMethod",
      "Type": "AWS::ApiGateway::Deployment",
      "Properties": {
        "RestApiId": {
          "Ref": "RestApi"
        },
        "StageName": "deploy"
      }
    },
    "TwilioSendConnectorLambdaFunction": {
      "DependsOn": "RestApi",
      "Type": "AWS::Lambda::Function",
      "Properties": {
        "Code": {
          "S3Bucket": "deflavia.me",
          "S3Key": {
            "Fn::Sub": "${BucketLambdaKey}/twilioSendConnector.zip"
          }
        },
        "Environment": {
          "Variables": {
            "BUCKET_NAME": {
              "Ref": "BucketName"
            },
            "BUCKET_PREFIX": {
              "Ref": "BucketMediaKey"
            },
            "TMP_DIR": "/tmp",
            "EXCH_USER": {
              "Ref": "SMSForwardUser"
            },
            "EXCH_PASSWORD": {
              "Ref": "SMSForwardUserPassword"
            },
            "EXCH_ENDPOINT": {
              "Fn::Sub": "https://${EXCHServerPrivateIp}/ews/exchange.asmx"
            },
            "TWILIO_ACCOUNT_SID": {
              "Ref": "TwilioAccountSid"
            },
            "TWILIO_AUTH_TOKEN": {
              "Ref": "TwilioAuthToken"
            },
            "TWILIO_NUMBER": {
              "Ref": "TwilioNumber"
            },
            "TWILIO_WEBHOOK": {
              "Fn::Sub": "https://${RestApi}.execute-api.${AWS::Region}.amazonaws.com/deploy/twilio/sms/message"
            }
          }
        },
        "FunctionName": "twilioSendConnector",
        "Handler": "index.handler",
        "MemorySize": 128,
        "Role": {
          "Fn::GetAtt": [
            "LambdaExecutionRole",
            "Arn"
          ]
        },
        "Runtime": "nodejs6.10",
        "Timeout": 300,
        "VpcConfig": {
          "SecurityGroupIds": [
            {
              "Ref": "LambdaEWSSG"
            }
          ],
          "SubnetIds": [
            {
              "Ref": "PrivateSubnetID"
            }
          ]
        }
      }
    },
    "HealthCheckLambdaFunction": {
      "DependsOn": "TwilioSendConnectorLambdaFunction",
      "Type": "AWS::Lambda::Function",
      "Properties": {
        "Code": {
          "S3Bucket": "deflavia.me",
          "S3Key": {
            "Fn::Sub": "${BucketLambdaKey}/healthCheck.zip"
          }
        },
        "Environment": {
          "Variables": {
            "EXCH_USER": {
              "Ref": "SMSForwardUser"
            },
            "EXCH_PASSWORD": {
              "Ref": "SMSForwardUserPassword"
            },
            "EXCH_ENDPOINT": {
              "Fn::Sub": "https://${EXCHServerPrivateIp}/ews/exchange.asmx"
            },
            "EXCH_MAILBOX": {
              "Fn::Sub": "${SMSDefaultUser}@${DomainDNSName}"
            },
            "TWILIO_WEBHOOK": {
              "Fn::Sub": "https://${RestApi}.execute-api.${AWS::Region}.amazonaws.com/deploy/twilio/sms/message"
            },
            "HEALTH_CHECK_USER_AGENT": {
              "Ref": "HealthCheckUserAgent"
            }
          }
        },
        "FunctionName": "healthCheck",
        "Handler": "index.handler",
        "MemorySize": 128,
        "Role": {
          "Fn::GetAtt": [
            "LambdaExecutionRole",
            "Arn"
          ]
        },
        "Runtime": "nodejs6.10",
        "Timeout": 300,
        "VpcConfig": {
          "SecurityGroupIds": [
            {
              "Ref": "LambdaEWSSG"
            }
          ],
          "SubnetIds": [
            {
              "Ref": "PrivateSubnetID"
            }
          ]
        }
      }
    },
    "HealthCheckEventRule": {
      "DependsOn": "HealthCheckLambdaFunction",
      "Type": "AWS::Events::Rule",
      "Properties": {
        "Name": {
          "Fn::Sub": "${DeploymentKey}-healthCheck"
        },
        "ScheduleExpression": "cron(0/1 * * * ? *)",
        "State": "ENABLED",
        "Targets": [
          {
            "Id": {
              "Fn::Sub": "${DeploymentKey}-healthCheck-target"
            },
            "Arn": {
              "Fn::GetAtt": [
                "HealthCheckLambdaFunction",
                "Arn"
              ]
            }
          }
        ]
      }
    },
    "HealthCheckLambdaFunctionInvokePermission": {
      "DependsOn": "HealthCheckEventRule",
      "Type": "AWS::Lambda::Permission",
      "Properties": {
        "FunctionName": {
          "Ref": "HealthCheckLambdaFunction"
        },
        "Action": "lambda:InvokeFunction",
        "Principal": "events.amazonaws.com",
        "SourceArn": {
          "Fn::GetAtt": [
            "HealthCheckEventRule",
            "Arn"
          ]
        }
      }
    },
    "ExchangeReceiveConnectorLambdaFunctionInvokePermission": {
      "DependsOn": "RestApi",
      "Type": "AWS::Lambda::Permission",
      "Properties": {
        "FunctionName": {
          "Ref": "ExchangeReceiveConnectorLambdaFunction"
        },
        "Action": "lambda:InvokeFunction",
        "Principal": "apigateway.amazonaws.com",
        "SourceArn": {
          "Fn::Sub": "arn:aws:execute-api:${AWS::Region}:${AWS::AccountId}:${RestApi}/*"
        }
      }
    }
  }
}